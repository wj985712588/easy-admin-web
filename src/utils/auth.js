import config from "@/settings";
import Cookies from "js-cookie";

const tokenKey = config.tokenKey;

/**
 * 获取accessToken
 * @returns {any}
 */
export function getToken() {
  return Cookies.get(tokenKey);
}

export function setToken(token) {
  Cookies.set(tokenKey, token);
}

/**
 * 移除accessToken
 */
export function removeToken() {
  Cookies.remove(tokenKey);
}
