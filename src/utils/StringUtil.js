const StringUtil = {
  removeSpace: (e) => {
    return e.replace(/(\s*)/g, "");
  },
  isEmpty: (e) => {
    return typeof e == "undefined" || e === null || e === "";
  },
  isBlank: (e) => {
    return StringUtil.isEmpty(e) || StringUtil.removeSpace(e).length === 0;
  },
  isNotEmpty: (e) => {
    return !StringUtil.isEmpty(e);
  },
  isNotBlank: (e) => {
    return !StringUtil.isBlank(e);
  },
};

export default StringUtil;
