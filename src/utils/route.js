import Layout from "@/layout/index";

export const loadView = (view) => {
  console.log("加载组件...");
  return () => import("@/views/" + view);
};

export const transformComponent = (routers) => {
  // 遍历后台传来的路由字符串，转换为组件对象
  return routers.filter((router) => {
    if (router.component) {
      if (router.component === "Layout") {
        // Layout组件特殊处理
        router.component = Layout;
      } else {
        const component = router.component;
        router.component = loadView(component);
      }
    }
    if (router.children != null && router.children && router.children.length) {
      router.children = transformComponent(router.children);
    } else {
      delete router["children"];
      delete router["redirect"];
    }
    return true;
  });
};
