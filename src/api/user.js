import request from "@/utils/request";

export function queryUser(data) {
  return request({
    url: "/user",
    method: "POST",
    data,
  });
}

export function getInfo() {
  return request({
    url: "/auth/info",
    method: "get",
  });
}
