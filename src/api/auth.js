import request from "@/utils/request";

export function login(data) {
  return request({
    url: "/auth/login",
    method: "POST",
    data,
  });
}

export function fetchAuthInfo() {
  return request({
    url: "/auth/info",
    method: "get",
  });
}

export function getCodeImg() {
  return request({
    url: "auth/code",
    method: "get",
  });
}

export function logout() {
  return request({
    url: "auth/logout",
    method: "delete",
  });
}

export function fetchRoutes() {
  return request({
    url: "auth/routes",
    method: "get",
  });
}
