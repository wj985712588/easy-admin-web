import { createRouter, createWebHashHistory } from "vue-router";
import Login from "../views/login/index";
import Layout from "@/layout";

export const constantRoutes = [
  {
    path: "/login",
    name: "Login",
    component: Login,
    hidden: true,
    meta: { title: "登录" },
  },
  {
    path: "/",
    component: Layout,
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        component: () =>
          import(/* webpackChunkName: "about" */ "../views/home.vue"),
        name: "Dashboard",
        meta: { title: "首页", icon: "dashboard", affix: true, noCache: true },
      },
    ],
  },
  {
    path: "/about",
    name: "About",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "",
        component: () =>
          import(/* webpackChunkName: "about" */ "../views/About.vue"),
        meta: { title: "关于" },
      },
    ],
  },
  {
    path: "/",
    name: "UserCenter",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "userCenter",
        component: () =>
          import(/* webpackChunkName: "about" */ "../views/user/center.vue"),
        meta: { title: "个人中心" },
      },
    ],
  },
  {
    path: "/error",
    component: Layout,
    name: "ErrorPages",
    hidden: true,
    meta: {
      title: "Error Pages",
      icon: "component",
    },
    children: [
      {
        path: "401",
        component: () => import("@/views/error-page/401"),
        name: "Page401",
        meta: {
          title: "401",
          icon: "component",
          noCache: true,
        },
      },
      {
        path: "404",
        component: () => import("@/views/error-page/404"),
        name: "Page404",
        meta: {
          title: "404",
          icon: "component",
          noCache: true,
        },
      },
    ],
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes: constantRoutes,
});

export default router;
