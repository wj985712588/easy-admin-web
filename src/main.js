import { createApp } from "vue";
import App from "./App.vue";
import ElementPlus from "element-plus";
import zhCN from "element-plus/es/locale/lang/zh-cn";
import "element-plus/theme-chalk/index.css";
import router from "./router";
import store from "./store";
import SvgIcon from "@/components/SvgIcon"; // svg component
import * as ElIconModules from "@element-plus/icons-vue";
import "@/permission"; // permission control
import "normalize.css/normalize.css"; // a modern alternative to CSS resets
import "@/assets/styles/index.scss"; // global css
import "@/assets/styles/element-plus.scss";
import "@/assets/styles/element-variables.scss";

const app = createApp(App);
app.use(store).use(router);
app.use(ElementPlus, {
  locale: zhCN,
});

// register el-icon
for (const iconName in ElIconModules) {
  if (Reflect.has(ElIconModules, iconName)) {
    const item = ElIconModules[iconName];
    app.component(iconName, item);
  }
}
// register component svg-icon
app.component("svg-icon", SvgIcon);
const req = require.context("@/icons/svg", false, /\.svg$/);
const requireAll = (requireContext) =>
  requireContext.keys().map(requireContext);
requireAll(req);
app.mount("#app");
