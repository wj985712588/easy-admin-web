import router from "./router";
import store from "@/store";
import Config from "@/settings";
import NProgress from "nprogress"; // progress bar
import "nprogress/nprogress.css"; // progress bar style
import { getToken } from "@/utils/auth";
import { fetchAuthInfo, fetchRoutes } from "@/api/auth";
import { transformComponent } from "@/utils/route";

const whiteList = ["/login"]; // no redirect whitelist

router.beforeEach((to, from, next) => {
  // start progress bar
  NProgress.start();

  if (to.meta.title) {
    document.title = to.meta.title + " - " + Config.title;
  }

  if (getToken()) {
    // 已登录
    if (to.path === "/login") {
      // 已登录
      next({ path: "/" });
      NProgress.done();
    } else {
      if (store.getters.userInfo.id) {
        // 用户已获取用户信息
        if (!store.getters.loadMenus) {
          loadMenus();
        }
        next();
      } else {
        fetchAuthInfo()
          .then((res) => {
            store.dispatch("user/saveUserInfo", res.data).then(() => {
              loadMenus(next, to);
              //next();
            });
          })
          .catch(() => {
            store.dispatch("logout").then(() => {
              // 为了重新实例化vue-router对象 避免bug
              location.reload();
            });
            next(`/login?redirect=${to.path}`);
            console.log("error when fetch auth info");
          });
      }
      NProgress.done();
    }
  } else {
    // 未登录
    if (whiteList.indexOf(to.path) !== -1) {
      // 请求地址在免登录白名单里
      next();
    } else {
      // 重定向到登录页
      next(`/login?redirect=${to.path}`);
      NProgress.done();
    }
  }
});

export const loadMenus = (next, to) => {
  fetchRoutes().then((res) => {
    store.dispatch("updateLoadMenus").then(() => {
      const asyncRoutes = transformComponent(res.data);
      store
        .dispatch("permission/generateRoutes", asyncRoutes)
        .then((accessedRoutes) => {
          accessedRoutes.forEach((e) => {
            console.log("添加路由...");
            router.addRoute(e);
          });
          if (next && to) {
            next({ ...to, replace: true });
          }
        });
    });
  });
};

router.afterEach(() => {
  // finish progress bar
  NProgress.done();
});
