import { createStore } from "vuex";
import { getToken, setToken, removeToken } from "@/utils/auth";
import settings from "@/store/module/settings";
import user from "@/store/module/user";
import app from "@/store/module/app";
import permission from "@/store/module/permission";

export default createStore({
  state: {
    token: getToken(),
    loadMenus: false,
  },
  getters: {
    token(state) {
      return state.token;
    },
    loadMenus(state) {
      return state.loadMenus;
    },
    userInfo(state) {
      return state.user;
    },
    permissionRoutes(state) {
      return state.permission.routes;
    },
    sidebar(state) {
      return state.app.sidebar;
    },
    device(state) {
      return state.app.device;
    },
  },
  mutations: {
    saveToken(state, token) {
      state.token = token;
    },
    setLoadMenus(state) {
      state.loadMenus = true;
    },
  },
  actions: {
    login({ commit }, loginInfo) {
      commit("saveToken", loginInfo.token);
      setToken(loginInfo.token);
      commit("user/saveUserInfo", loginInfo.userInfo);
    },
    logout({ commit }) {
      commit("saveToken", "");
      removeToken();
      commit("user/saveUserInfo", {});
    },
    updateLoadMenus({ commit }) {
      commit("setLoadMenus");
    },
  },
  modules: {
    settings: settings,
    user: user,
    app,
    permission,
  },
});
