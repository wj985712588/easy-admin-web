export default {
  namespaced: true,
  state: {
    id: "",
    name: "",
    nickName: "",
    avatar: "",
    roles: [],
  },
  mutations: {
    saveUserInfo(state, userInfo) {
      state.id = userInfo.id;
      state.name = userInfo.name;
      state.avatar = userInfo.avatar;
      state.nickName = userInfo.nickName;
    },
  },
  actions: {
    saveUserInfo({ commit }, userInfo) {
      commit("saveUserInfo", userInfo);
    },
  },
};
